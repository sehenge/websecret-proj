<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::prefix('departments')->group(function() {
    Route::get('all', [DepartmentController::class, 'index']);
    Route::post('create', [DepartmentController::class, 'store']);
    Route::put('edit', [DepartmentController::class, 'update']);
    Route::delete('delete/{id}', [DepartmentController::class, 'destroy']);
});

Route::prefix('employees')->group(function() {
    Route::get('all', [EmployeeController::class, 'index']);
    Route::post('create', [EmployeeController::class, 'store']);
    Route::put('edit', [EmployeeController::class, 'update']);
    Route::delete('delete/{id}', [EmployeeController::class, 'destroy']);
});
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $hidden = ['pivot', 'updated_at', 'created_at', 'employees'];

    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }
}

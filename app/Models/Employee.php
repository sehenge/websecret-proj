<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $hidden = ['pivot', 'updated_at', 'created_at'];

    public function departments()
    {
        return $this->belongsToMany(Department::class);
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->firstname} {$this->middlename} {$this->lastname}";
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate(10)->items();

        foreach ($employees as &$employee) {
            $employee['fullname'] = $employee->getFullNameAttribute();
            unset($employee['firstname']);
            unset($employee['lastname']);
            unset($employee['middlename']);
            $tmp = [];
            foreach ($employee->departments as $department) {
                $tmp[] = $department['name'];
            }
            $employee['departments_name'] = $tmp;
            unset($employee['departments']);
        }

        return $employees;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = Validator::make($request->all(),[
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'middlename' => 'required|max:255',
            'salary' => 'required|integer'
        ]);

        if (!$validated->fails()) {
            $employee = new Employee();
            $employee->firstname = $request->firstname;
            $employee->lastname = $request->lastname;
            $employee->middlename = $request->middlename;
            $employee->gender = $request->gender;
            $employee->salary = $request->salary;

            try {
                $department_relations = json_decode($request->departments);
                if (is_array($department_relations) && (sizeof($department_relations) > 0)) {
                    $employee->save();
                    foreach ($department_relations as $department_id) {
                        $employee->departments()->sync($department_id, false);
                    }
                } else {
                    return response()->json(['error' => 'Count of departments should be at least 1 or more']);
                }
            } catch (Exception $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        } else {
            return response()->json($validated->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $employee = Employee::find($request['id']);
            $employee->firstname = $request['firstname'] ? $request['firstname'] : $employee->firstname;
            $employee->lastname = $request['lastname'] ? $request['lastname'] : $employee->lastname;
            $employee->middlename = $request['middlename'] ? $request['middlename'] : $employee->middlename;
            $employee->gender = $request['gender'] ? $request['gender'] : $employee->gender;
            $employee->salary = $request['salary'] ? $request['salary'] : $employee->salary;

            $departments = json_decode($request['departments']);
            if (sizeof($departments) > 0) {
                foreach ($departments as $department_id) {
                    $employee->departments()->sync($department_id, false);
                }
                $employee->save();
            } else {
                return response()->json(['error' => 'Count of departments should be at least 1']);
            }

        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $employee = Employee::find($id);
        $employee->delete();
    }
}

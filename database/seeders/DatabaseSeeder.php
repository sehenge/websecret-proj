<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\DepartmentEmployee;
use App\Models\Employee;
use Database\Factories\DepartmentFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Department::factory(15)->create();
        Employee::factory(60)->create();
        DepartmentEmployee::factory(60)->create();
    }
}

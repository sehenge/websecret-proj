<?php

namespace Database\Factories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = $this->faker->randomElement(['male', 'female']);
        return [
            'firstname' => $this->faker->firstName($gender),
            'lastname' => $this->faker->lastName(),
            'middlename' => $this->faker->firstName($gender),
            'gender' => $gender,
            'salary' => $this->faker->numberBetween(100, 2000)
        ];
    }
}
